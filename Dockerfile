FROM centos:7
MAINTAINER "The Quattro Project Team"

ENV DOCKER_CLIENT_VERSION 1.13.1-91.git07f3374.el7.centos

RUN yum -y install docker-client-${DOCKER_CLIENT_VERSION}.x86_64 && yum clean all
