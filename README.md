# CentOS docker

This CentOS image can be used in pipelines to connect to the Docker daemon when
not using the official Alpine based `docker:latest` image.
